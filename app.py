from flask import Flask,render_template,redirect,session
from flask import request
import mysql.connector
import random
import smtplib

db=mysql.connector.connect(username="root",password="Admin",database="blooddb")
cur=db.cursor()

s = smtplib.SMTP("smtp.gmail.com", 587)
s.starttls()
s.login("gmanikanta96@gmail.com", 'vtlcmllthiunujso')

def generatePassword():
    s = ""
    while len(s) <= 10:
        x = random.randint(0, 255)
        if ((ord("A") <= x <= ord("Z")) or (ord("a") <= x <= ord("z")) or (
                ord("0") <= x <= ord("9"))):
            s += chr(x)
    return s

def getbloodgroups():
    l=[]
    q="select bname from stock"
    cur.execute(q)
    d=cur.fetchall()
    for i in d:
        l.append(i[0])
    return l
def getquantity(group):
    query = "select qty from stock where bname='{}'".format(group)
    cur.execute(query)
    data = cur.fetchall()
    return int(data[0][0])


def setQuantity(group,qty):
    query = "update stock set qty={} where bname='{}'".format(qty,group)
    cur.execute(query)
    db.commit()
def setstatus(id,s):
    query = "update user_log set status='{}' where logid='{}'".format(s,id)
    cur.execute(query)
    db.commit()

def getemail(id):
    query = "select email from user where id={}".format(id)
    cur.execute(query)
    data = cur.fetchall()
    return data[0][0]




app = Flask(__name__)


@app.route("/")
def welcome():
    return render_template("index.html")

@app.route("/adminLogin")
def adminLogin():
    return render_template("adminLogin.html")

@app.route("/user")
def user():
    return render_template("userreg.html")

@app.route("/userLogin")
def useLoginr():
    return render_template("userLogin.html")

@app.route("/addgroup")
def addgroup():
    return render_template("addgroup.html")

@app.route("/userhome")
def userhome():
    return render_template("UserHomePage.html")

@app.route("/admin")
def adminhome():
    return render_template("adminHomePage.html")


@app.route("/adminLoginDB", methods=['POST'])
def adminLoginDB():
    un = request.form['uname']
    pwd = request.form['pwd']
    msg=""
    if un == 'admin' and pwd == 'Admin':
        return render_template("adminHomePage.html")
    else:
        msg="Invalid credentials"
        return render_template("adminLogin.html",m=msg)




@app.route("/stock")
def showStock():
    q="select *from stock"
    cur.execute(q)
    data=cur.fetchall()
    return render_template("showstock.html",d=data)


@app.route("/addgroupdb",methods=['POST'])
def addgroupdb():
    n=request.form.get("bname")
    q=request.form.get("qty")
    q="insert into stock(bname,qty) values('{}',{})".format(n.upper(),q)
    cur.execute(q)
    db.commit()
    return redirect("/stock")

@app.route("/updateqty",methods=['POST'])
def updateqty():
    id=request.form.get("id")
    q="select *from stock where id="+str(id)
    cur.execute(q)
    data=cur.fetchall()
    return render_template("editgroup.html",d=data)

@app.route("/updatestockdb",methods=['POST'])
def updatestockdb():
    i=request.form.get("id")
    n=request.form.get("bname")
    q=request.form.get("qty")
    query="update stock set bname='{}',qty={} where id={}".format(n,q,i)
    cur.execute(query)
    db.commit()
    return redirect("/stock")

@app.route("/deletegroup",methods=['POST'])
def deletegroup():
    i=request.form.get("id")
    q="delete from stock where id="+str(i)
    cur.execute(q)
    db.commit()
    return redirect("/stock")

@app.route("/userRegDB", methods=['POST'])
def UserRegDB():
    name = request.form['name']
    g=request.form['g']
    dob=request.form['dob']
    bg=request.form['bg']
    mail = request.form['email']
    mobile= request.form['mobile']
    address = request.form['addr']
    username = request.form['uname']
    password = generatePassword()
    msg = "Your password is " + password
    s.sendmail("gmanikanta96@gmail.com",mail, msg)

    sql ="insert into user(name,gender,dob,bgroup,email,mobile,address,uname,password) values('{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(name,g,dob,bg,mail,mobile,address,username,password)
    cur.execute(sql)
    db.commit()
    return render_template("userLogin.html")

@app.route("/UserLoginDB", methods=['POST'])
def UserLoginDB():
    un = request.form.get('uname')
    pwd = request.form.get('pwd')
    sql = "SELECT * FROM user WHERE uname='" + un + "' and password='" + pwd + "'"
    cur.execute(sql)
    d = cur.fetchall()
    msg=""
    if len(d) > 0:
        session['userid'] = d[0][0]
        session['name']=d[0][1]
        msg="Hi,"+d[0][1]
        return render_template("UserHomePage.html",m=msg)
    else:
        msg="Invalid credentials"
        return render_template("userLogin.html",m=msg)

@app.route("/UserLoginDB")
def Userprofile():
    uid = session['userid']
    q = "select * from user where id={}".format(uid)
    cur.execute(q)
    data = cur.fetchall()
    return render_template("profile.html", d=data)

@app.route("/updateProfile", methods=['POST'])
def updateProfile():
    uid = session['userid']
    cur.execute("select * from user where id=" + str(uid))
    data = cur.fetchall()
    return render_template("updateProfile.html", d=data)

@app.route("/updateProfileDB",methods=['POST'])
def updateProfileDB():
    id= request.form['v1']
    name = request.form['v2']
    g = request.form['v3']
    dob = request.form['v4']
    bg = request.form['v5']
    mail = request.form['v6']
    mobile = request.form['v7']
    address = request.form['v8']
    username = request.form['v9']
    password = request.form['v10']
    sql = "update user set name='{}',gender='{}',dob='{}',bgroup='{}',email='{}',mobile='{}',address='{}',uname='{}',password='{}' where id={}".format(
        name, g, dob, bg, mail, mobile, address, username, password,id)
    cur.execute(sql)
    db.commit()
    q = "select * from user where id={}".format(id)
    cur.execute(q)
    data = cur.fetchall()
    return render_template("profile.html", d=data)

@app.route("/showusers")
def showusers():
    q="select *from user"
    cur.execute(q)
    data=cur.fetchall()
    return render_template("showusers.html",d=data)
@app.route("/removeuser/<int:id>",)
def removeuser(id):
    sql="delete from user where id="+str(id)
    cur.execute(sql)
    db.commit()
    return redirect("/showusers")

@app.route("/donate")
def donateblood():
    id=session['userid']
    name=session['name']
    type="donate"
    d=[id,name,type]

    return render_template('blooddonate.html',d=d)

@app.route("/requestb")
def requestblood():
    id=session['userid']
    name=session['name']
    type="request"
    d=[id,name,type]

    return render_template('bloodrequest.html',d=d)

@app.route("/userlog",methods=['POST'])
def userlog():

    id=request.form.get("v1")
    name= request.form.get("v2")
    type = request.form.get("v3")
    bg= request.form.get("v4").upper()
    u=int(request.form.get("v5"))
    reason = request.form.get("v6")
    status="pending"
    if type=="request":
        if bg not in getbloodgroups() or u>getquantity(bg):
            status="not avilable"
    query="insert into user_log(id,name,type,bgroup,units,reason,status) values({},'{}','{}','{}',{},'{}','{}')".format(id,name,type,bg,u,reason,status)
    cur.execute(query)
    db.commit()
    return render_template("UserHomePage.html")

@app.route("/showlog")
def showlog():
    q = "select * from user_log"
    cur.execute(q)
    data = cur.fetchall()
    return render_template("userlog.html", d=data)

@app.route("/approve/<int:id>")
def approve(id):
    q = "select * from user_log where logid="+str(id)
    cur.execute(q)
    data= cur.fetchall()
    rq=data[0][5]
    bq=getquantity(data[0][4])
    t=data[0][3]
    if t=="donate":
        setQuantity(data[0][4],(bq+rq))
    elif t=="request":
        setQuantity(data[0][4],(bq-rq))
    setstatus(id, "Approved")
    id=data[0][1]
    msg = "Your request approved"
    mail=getemail(id)
    s.sendmail("gmanikanta96@gmail.com",mail,msg)
    return redirect("/showlog")

@app.route("/reject/<int:id>")
def reject(id):
    q = "select * from user_log where logid=" + str(id)
    cur.execute(q)
    data = cur.fetchall()
    setstatus(id,"Rejected")
    id = data[0][1]
    msg = "Your request rejected"
    mail = getemail(id)
    s.sendmail("gmanikanta96@gmail.com", mail, msg)
    return redirect("/showlog")

@app.route("/usernot")
def usernot():
    uid = session['userid']
    q = "select *from user_log where id={}".format(uid)
    cur.execute(q)
    data = cur.fetchall()
    return render_template("usernotification.html", d=data)

@app.route("/logout")
def logout():
    return render_template("index.html")

if __name__ == '__main__':
    app.secret_key="abc"
    app.run(port=9000,debug=True)

